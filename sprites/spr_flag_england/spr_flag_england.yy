{
    "id": "00cf21f0-e5c9-4073-a1b0-aa8aad2ef66b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flag_england",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 149,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "abbf7d94-f876-4e0e-a034-f34ab5501005",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00cf21f0-e5c9-4073-a1b0-aa8aad2ef66b",
            "compositeImage": {
                "id": "25ec4c18-18fd-42b3-a5ea-ce7f1df7f32a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abbf7d94-f876-4e0e-a034-f34ab5501005",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4a823ee-0e15-44f3-ba2b-24fac40874b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abbf7d94-f876-4e0e-a034-f34ab5501005",
                    "LayerId": "16d34e20-936b-4459-81e5-f95c8e6a305a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "16d34e20-936b-4459-81e5-f95c8e6a305a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00cf21f0-e5c9-4073-a1b0-aa8aad2ef66b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}