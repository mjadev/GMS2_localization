{
    "id": "4ec58761-4dfa-4ee7-9594-6b2ff54a65a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flag_portugal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 149,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2efa6310-28e6-427e-9d68-a716af4eaf12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ec58761-4dfa-4ee7-9594-6b2ff54a65a9",
            "compositeImage": {
                "id": "8a7362a0-1dbe-44a2-a3b2-7a057c1be2c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2efa6310-28e6-427e-9d68-a716af4eaf12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "476da846-f552-4b7c-82c6-adfa3acbfb0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2efa6310-28e6-427e-9d68-a716af4eaf12",
                    "LayerId": "92787261-6cd5-4a85-82f0-681855d3f66f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "92787261-6cd5-4a85-82f0-681855d3f66f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ec58761-4dfa-4ee7-9594-6b2ff54a65a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}