{
    "id": "4e09f4b4-f189-4cb1-a12f-376d0c76e509",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d71aa82-ac0a-4b33-88d2-8ded94d2fd68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e09f4b4-f189-4cb1-a12f-376d0c76e509",
            "compositeImage": {
                "id": "14ef76aa-9fef-44ef-979f-b094474aa430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d71aa82-ac0a-4b33-88d2-8ded94d2fd68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "221832d6-cb93-4648-8ac7-5d7a844348aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d71aa82-ac0a-4b33-88d2-8ded94d2fd68",
                    "LayerId": "46fa6418-9f04-4525-8c0c-e310d1cc25ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "46fa6418-9f04-4525-8c0c-e310d1cc25ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e09f4b4-f189-4cb1-a12f-376d0c76e509",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}