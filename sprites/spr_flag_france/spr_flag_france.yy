{
    "id": "004c5ea2-4a70-4210-97e8-ace5a6f35e7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flag_france",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 149,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3449a13a-3358-4670-ad54-69f2261d0b0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004c5ea2-4a70-4210-97e8-ace5a6f35e7d",
            "compositeImage": {
                "id": "3572725d-90aa-444c-a2be-1c3489299863",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3449a13a-3358-4670-ad54-69f2261d0b0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b2ed80e-f5b1-42d6-a842-4985ddab676b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3449a13a-3358-4670-ad54-69f2261d0b0d",
                    "LayerId": "ebad3eaa-0910-4255-ae98-42f93c113bdd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "ebad3eaa-0910-4255-ae98-42f93c113bdd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "004c5ea2-4a70-4210-97e8-ace5a6f35e7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}