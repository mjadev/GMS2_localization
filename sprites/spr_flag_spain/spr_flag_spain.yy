{
    "id": "4218c1d6-5650-4674-a635-eafb8241c404",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flag_spain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 149,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7564b90a-5d53-4512-b627-c5933cc88cd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4218c1d6-5650-4674-a635-eafb8241c404",
            "compositeImage": {
                "id": "6245c719-07c5-4c32-9a48-60c72a9f57e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7564b90a-5d53-4512-b627-c5933cc88cd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "868c3822-9f58-4204-a86c-9d81e1cf567e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7564b90a-5d53-4512-b627-c5933cc88cd6",
                    "LayerId": "b5a654e3-6c04-4aa2-8bec-a1e9d4b22d6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "b5a654e3-6c04-4aa2-8bec-a1e9d4b22d6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4218c1d6-5650-4674-a635-eafb8241c404",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}