{
    "id": "8ed8bae7-d067-4ef9-a721-9a9a70713987",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_location_test_1",
    "eventList": [
        {
            "id": "be34d292-bbbf-4baf-89e3-da105c5e5119",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8ed8bae7-d067-4ef9-a721-9a9a70713987"
        },
        {
            "id": "9d6e424c-d6e4-46ec-a750-7ff42d5ba92a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8ed8bae7-d067-4ef9-a721-9a9a70713987"
        },
        {
            "id": "09c7db5e-5a15-4fe3-a999-95abdacb1bd0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8ed8bae7-d067-4ef9-a721-9a9a70713987"
        },
        {
            "id": "355a2b98-35fd-40b9-94b0-cb63051e657b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "8ed8bae7-d067-4ef9-a721-9a9a70713987"
        },
        {
            "id": "439b36d2-3677-4ce1-b88d-3494a3f31f6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 5,
            "m_owner": "8ed8bae7-d067-4ef9-a721-9a9a70713987"
        },
        {
            "id": "90b0f1ce-01c4-413e-98c2-0d4a8bc87690",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "8ed8bae7-d067-4ef9-a721-9a9a70713987"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "004c5ea2-4a70-4210-97e8-ace5a6f35e7d",
    "visible": true
}