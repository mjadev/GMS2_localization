/// @description Insert description here
// You can write your code in this editor

// select next language... should be based on the result of json_decode
//TODO optimize this
switch (global.language)
{
	case game_lang_e.english:
		global.language = game_lang_e.french;
		break;
		
	case game_lang_e.french:
		global.language = game_lang_e.spanish;
		break;
		
	case game_lang_e.spanish:
		global.language = game_lang_e.portuguese;
		break;
		
	case game_lang_e.portuguese:
		global.language = game_lang_e.english;
		break;					
}

lang_choice = scr_gameloc_select_lang(global.language);
scr_loc_set_current_lang(lang_choice);
sprite_index = scr_gameloc_flag_select(global.language);
