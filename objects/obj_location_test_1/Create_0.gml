/// @description Test Location number 1
// In this test, languages are well known when compiling the game
// There is one flag sprite for each supported language
// Left-Click on the flag to select another language

// initialize test
// game language (localization)
enum game_lang_e {
   english,
	 french,
	 spanish,
	 portuguese,
}
// starting language
global.language = game_lang_e.french;

// select default language
// lang_choice is a string: it has to correspond to JSON file keys
lang_choice = scr_gameloc_select_lang(global.language);
sprite_index = scr_gameloc_flag_select(global.language);

// init localization
scr_loc_init("loc_the_flood.json");

// TEST WITH CSV
//scr_loc_read_csv_file("test_csv.csv");
