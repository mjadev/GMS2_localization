{
    "id": "0d9f02ca-cc5d-449f-aa7a-8ed27096baf8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_test_2_prev_lang",
    "eventList": [
        {
            "id": "f047d827-4303-463b-a57b-d9baf2479505",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "0d9f02ca-cc5d-449f-aa7a-8ed27096baf8"
        },
        {
            "id": "9df773d8-f833-4323-ac1b-86ff7b3a7525",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0d9f02ca-cc5d-449f-aa7a-8ed27096baf8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "4e09f4b4-f189-4cb1-a12f-376d0c76e509",
    "visible": true
}