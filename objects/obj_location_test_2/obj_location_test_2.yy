{
    "id": "e721ee8a-fd9a-49db-b309-2e5f505555a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_location_test_2",
    "eventList": [
        {
            "id": "da1ddf5e-847a-4fcf-8e63-c32c1b567a6a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e721ee8a-fd9a-49db-b309-2e5f505555a5"
        },
        {
            "id": "19f0603a-6d2d-4c82-a3ea-cc6a1a6bdba7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e721ee8a-fd9a-49db-b309-2e5f505555a5"
        },
        {
            "id": "759fc125-4282-4908-9ce0-5ee2a8e111e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "e721ee8a-fd9a-49db-b309-2e5f505555a5"
        },
        {
            "id": "3bc7a3a8-eaa7-44e7-a35c-62f3574ca8f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 78,
            "eventtype": 9,
            "m_owner": "e721ee8a-fd9a-49db-b309-2e5f505555a5"
        },
        {
            "id": "3a82876e-ddc1-422f-b822-60b167cce319",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 80,
            "eventtype": 9,
            "m_owner": "e721ee8a-fd9a-49db-b309-2e5f505555a5"
        },
        {
            "id": "49a8c6e4-b7b7-42d6-a632-88d76a328271",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "e721ee8a-fd9a-49db-b309-2e5f505555a5"
        },
        {
            "id": "79f54644-7b14-4c74-be96-ed9d6e1d79fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "e721ee8a-fd9a-49db-b309-2e5f505555a5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}