/// @description Test Location number 2
// In this test, number languages is unknown when compiling the game
// Click on next (N) or previous (P) to change the current language (cycle between
// all available languages)


// initialize test

// init localization
scr_loc_init("loc_the_flood.json");

// TEST WITH CSV
//scr_loc_read_csv_file("test_csv.csv");
