{
    "id": "55fd4e0b-6713-4d1b-8d4d-d0a5f83020ac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_test_2_next_lang",
    "eventList": [
        {
            "id": "a34df801-be09-409c-8b2e-5df14c05ff7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "55fd4e0b-6713-4d1b-8d4d-d0a5f83020ac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "4e09f4b4-f189-4cb1-a12f-376d0c76e509",
    "visible": true
}