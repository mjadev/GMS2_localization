{
    "id": "827a93ac-ed80-4a12-9db7-9966b9f543e0",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_test_menu",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "62ec837a-bd1d-4f2b-977c-e1cd6cac29f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 80,
                "y": 170
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "5c23b94c-3c3c-400b-aa2f-c3f34b315a46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 53,
                "y": 194
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "bd85ed13-0477-4645-a073-ed424d492f7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 32,
                "y": 194
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a7d07de8-bb78-449c-974e-29468ee667a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 165,
                "y": 74
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "45b7eb9c-4f9c-467c-9a68-187a0b86fd7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 229,
                "y": 74
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "33e2bdd1-0ff6-4bb5-811d-6e47572554e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "46418d96-dd5e-4dd1-b56e-681a3c00ca54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 160,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e0b54224-1de0-45aa-baa0-c81ba68f338e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 8,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 109,
                "y": 194
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "62372849-7525-417e-8673-e28b5db155b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 94,
                "y": 170
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "3c9ed6bb-a19b-47f7-9ec6-e3eb6b909d86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 87,
                "y": 170
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "180513d3-ca3d-4e9c-81a7-874cd5ee5125",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 194
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "50ea9f0b-b71d-4d78-bb38-b382b23b9c7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 16,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 12,
                "y": 122
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "7f84710f-3035-417e-a0d1-ec821fe4be34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 21,
                "y": 194
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "2ff149ad-0d72-4998-8162-354612bc8fb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 207,
                "y": 170
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "45ec512c-db45-4d27-8d02-81e03d088245",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 49,
                "y": 194
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "deeacd13-ab03-4762-b6bd-d6d25144ce40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 7,
                "x": 29,
                "y": 170
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "30597910-7bd1-4407-bf7b-fde8eb1ac4eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 79,
                "y": 122
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2f86b06d-f40b-40b8-a625-5547e2ea8f54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 2,
                "shift": 11,
                "w": 5,
                "x": 134,
                "y": 170
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "6ee11fd2-baf0-421c-aa85-7ac0ad2acfdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 74,
                "y": 98
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "532058c9-2900-41cc-9f09-9b3cd156864b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 145,
                "y": 122
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "6b286abe-e6d8-4b58-8b3d-68fbf9b07dbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 134,
                "y": 98
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "001a4d8e-8501-46ee-9eae-b09046f34c87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "46e46465-a48f-4b71-ab14-3b88f9ad5bc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 35,
                "y": 146
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "4bf1d5f8-36ce-4999-a610-3875beafeeb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 244,
                "y": 122
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "e2fb514c-3caf-4421-8e42-4265c9154881",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 233,
                "y": 122
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f8b0029e-ca5f-4b04-b5cd-e3568b5acca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 178,
                "y": 122
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3eb0def6-bfa5-4e0d-ae7d-283ca87977bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 69,
                "y": 194
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "67d202a3-4afa-46e6-8ec8-60dd4725dd92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 20,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 45,
                "y": 194
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5bbda5c6-3911-4531-af77-a4fb45097a06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 242,
                "y": 146
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2e72afbd-c879-4cac-be11-ca3ba2b79830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 13,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 58,
                "y": 170
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fc17f806-572a-4a33-a0f3-18089b166f6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 170
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3c4ea705-c28d-41d7-9516-cedfc4e28b86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 123,
                "y": 122
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "03b3e1d5-9af2-4449-a3f4-77ec3df66ee8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "039cfa81-3265-4e3d-bcb9-75e24685cd92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7d2331b7-6439-4abf-b94b-c5dd9b5021c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 206,
                "y": 98
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "78e4e05d-31d6-45c9-a697-e794ae281961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 107,
                "y": 50
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "2a2a201d-5ee8-4d7c-9766-7da3fc29de3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 48,
                "y": 74
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1fc7e663-3382-4ef4-af7f-3de2ab270b67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 182,
                "y": 98
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "55600c47-17a6-46c2-b2cf-04efede59099",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 134,
                "y": 122
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "92e078bd-a6c1-4a96-86e3-310fa9769c19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 77,
                "y": 50
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b5ac2f55-fb42-44ca-a7b6-2398bb26ae93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 191,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "45169653-8d22-4c08-9469-9853ac817059",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 61,
                "y": 194
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e7fce7fe-cc25-4b47-beb7-41c098bb685c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 112,
                "y": 146
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5d12673f-3c40-406a-a938-11be4737543f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 204,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "62a09cf3-f3a1-4963-9e9e-35b8a23f1e26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 122,
                "y": 146
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "8a37dd59-d9c0-439f-b5ef-bfda6f458caf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "891bcea2-4768-43c6-8284-64b209b30703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 178,
                "y": 74
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "904fce9f-aa5c-4991-a34b-803e0a2cb6db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 201,
                "y": 26
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "3f819978-9754-4728-bf6b-cf3dc9f6a57b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 218,
                "y": 98
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "7837da53-4b5a-477f-8fa9-1eedee8bb081",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 134,
                "y": 26
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d2e659dc-f775-4c72-ae27-e0d8d42b6c55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 146,
                "y": 50
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2da72ee9-02c1-4c5f-aa28-d843bdfbb574",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 152,
                "y": 74
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b3d8dd9d-4f5b-4a2c-ab1a-fdec5fae45f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 230,
                "y": 98
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a379707d-1490-41ce-a26f-643ca4e9a19a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 139,
                "y": 74
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "1410468f-1929-49ba-b30e-aa024154c4cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "2201cc44-0da2-43dc-afbd-9b015efc08e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "30758b8f-2e90-4af7-9f0f-08f97f14806f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 62,
                "y": 50
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "67b19d39-1c08-4cfd-8376-bc220a5144eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 188,
                "y": 50
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c04268db-1093-4e01-99ab-6e350ec76335",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 100,
                "y": 74
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "96bba36e-30b3-4de3-be9c-73b9f82c7c90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 167,
                "y": 170
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "fa5dfe04-ba3c-4980-b4c5-90a54037fafd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 38,
                "y": 170
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "b32594a8-077f-4498-8618-e0c355d31f7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 161,
                "y": 170
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "15d1dba3-71c5-4381-abc1-db222096f8a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 182,
                "y": 170
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "103bbafc-f61c-4806-8e7a-e77f7e287b48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 175,
                "y": 26
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "0d3a1a55-488d-45b7-b056-9c302efb2ee4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 6,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 99,
                "y": 194
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f1825cb7-b3e5-4a34-8852-7efd5b00a667",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 242,
                "y": 98
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "937c910a-8578-44ec-bc1b-e5de751a7f9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 156,
                "y": 122
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e1744679-525f-49ec-ae31-9491e4b1ca8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 192,
                "y": 146
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e8653c93-6707-4304-8bf4-3c2a46e51c68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 200,
                "y": 122
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "76eac2f6-050f-4e88-9001-137372f702ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 211,
                "y": 122
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3948f526-e05a-4109-9b1c-806feadad1c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 101,
                "y": 170
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "365258b6-1c24-438b-9052-abab8d769442",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 122,
                "y": 50
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "290dde96-ba44-42dc-a7a3-9da73fd26005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 222,
                "y": 146
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7b89c2cf-b4c7-4296-a5a0-216d0bf2da67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 57,
                "y": 194
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4303b46c-f038-4c65-81ff-dd001df7e1bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 155,
                "y": 170
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d604e134-6d6f-4452-b6fe-2f616c76f91c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 202,
                "y": 146
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c67a4dea-924a-41ef-812d-3eb04ae186c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 65,
                "y": 194
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "10d201b2-3c95-46b2-a8d7-0ea0263b7b00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 86,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "28d4aa7b-0b95-46d5-93f8-63dfee5f1ea6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 172,
                "y": 146
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "06256a8f-73fe-4bf9-b5be-b65f0c9c9845",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 189,
                "y": 122
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "573e9e56-2979-47b4-9a02-7a4ecce430d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "da0f626d-fa81-46d4-9e44-9afece102604",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 74
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "3df21884-5986-4267-bbc0-0310b900f344",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 141,
                "y": 170
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "bb4d6b74-673a-44a8-8677-4970121a36d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 212,
                "y": 146
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "31df7047-e95b-4e78-9256-02106072c302",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 148,
                "y": 170
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "89a6e123-1d90-4411-a245-7c68ced7be69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 142,
                "y": 146
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "716ab1d5-b992-414c-9f78-24544637233a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 74,
                "y": 74
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "8c464357-dcd4-4e19-b96d-c953408803c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 53,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2e111da1-376b-441e-88eb-689bb31bb3e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 240,
                "y": 74
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "752d796b-5b6b-4264-b782-527cadb345b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 188,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "11de9cc5-b478-4e6c-8722-4ab5ca4bf422",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 167,
                "y": 122
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f6dcce2e-ba22-40ba-87b5-4a1fb18a0ba6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 13,
                "y": 170
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3cccf4bf-caf7-4b23-a62f-4c3b0ee64790",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 17,
                "y": 194
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6b46f593-0a1e-4585-8efb-235461cb5b0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 21,
                "y": 170
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f6e1e16b-66ce-473e-9c97-c294543ca365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 13,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 47,
                "y": 170
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "87d09c49-bb43-40e0-83e5-eb6a1bc91f16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 0,
                "x": 113,
                "y": 194
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "66f9666e-2af5-42f2-9147-8e879c01fbea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 21,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 9,
                "y": 194
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "2052c1b7-0ff4-4ac0-86cf-c59f81f3a434",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 74
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "83354f76-d26e-4c87-a73c-da95c65d2d37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 146
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "b4aa8afa-4931-475d-997f-97e1b1bec583",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 58,
                "y": 146
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "9f136275-35d7-41db-b121-db5e12d02c66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 158,
                "y": 98
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "e6261286-d847-48d0-ab56-1d089e935880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 21,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 13,
                "y": 194
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "e0c8c7cc-0679-484c-83fd-cd750ec72443",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 214,
                "y": 50
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "c44f93fc-d81b-43a3-8af3-17e45daea4ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 5,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 73,
                "y": 194
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "cd0aa332-3ad2-4358-af4c-bcda96e9cda4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 118,
                "y": 26
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "519c23b4-2bd8-4a66-9797-b42f4a53770c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 173,
                "y": 170
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "deb37c75-53b4-4540-87f7-b0775fc4fad5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 16,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 81,
                "y": 146
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "dfdd85cc-85d1-4c75-bc1a-bca5cb830b81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 13,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 69,
                "y": 170
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "4c5c1e2f-bfd1-41ef-bff3-bcd116708d90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 200,
                "y": 170
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "3ecdc4bf-cdd1-4672-91b0-43506d375a83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 102,
                "y": 26
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "a0a0d326-b6e5-452f-bc4d-5e7bf551c3fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 2,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 81,
                "y": 194
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "d223646f-83e1-4c9e-8c52-250096202729",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 25,
                "y": 194
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "405f1354-727a-467f-bf84-cf9f865ca07e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 146,
                "y": 98
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "438b3431-3df9-427f-9b43-fa3926565087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 227,
                "y": 170
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "e0cc30c7-c744-4442-8131-d263ec8402e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 219,
                "y": 170
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "333e2bdd-8f10-4ec4-8174-72186caec469",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 6,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 104,
                "y": 194
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "5be09e51-c77f-427b-8daf-79fa7f230932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 236,
                "y": 50
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "d4d50e50-3719-45d2-913c-68419322863c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 202,
                "y": 50
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "18a3a93b-df34-4c76-b292-45afd61aca7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 11,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 95,
                "y": 194
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "4e88cf2f-8d93-4460-8c13-a497461f3e56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 214,
                "y": 170
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "69c610b5-4386-4835-b8a5-37b56a296fd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 39,
                "y": 194
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "be5fbe7b-aaba-4865-8596-fd112125ac12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 191,
                "y": 170
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "ece9c76c-da19-4ccb-b7d5-4667417c998f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 16,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 70,
                "y": 146
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "94dc7f05-502e-4846-bded-c058b12d531d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 17,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "9894568a-3004-4ee8-a3a1-64ace86741e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 17,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "48ee1726-8108-4511-9d5d-efa5e6132070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 17,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "ca83be5f-437b-48f3-899d-fd5709c3cfd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 134,
                "y": 50
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "8776f11b-ce75-4c7d-b9e2-0fe181563879",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 17,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "c3fbd20c-b5ce-4c42-9ad9-a12f01fd6179",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 17,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 36,
                "y": 26
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "77587e1c-8e48-4554-93b1-3db910b0620b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 17,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "b4cc7281-7244-4eab-b11d-b711f9eef1db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 17,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "5623c02d-445b-4ced-b129-d60689192d36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 17,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "e0ad8b61-5ae0-47a8-b397-0b4a9496b889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 17,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 19,
                "y": 26
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "5700d8a9-213d-4a87-b54d-40867847d3fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 17,
                "offset": -1,
                "shift": 19,
                "w": 19,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "baec3c28-1ef0-427b-8073-8d17df91c32b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "620700d9-4c41-4cc3-8709-e024c5adadca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 17,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 86,
                "y": 98
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "7654fdf1-de27-4ef4-bcc8-d00f6d4a0804",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 17,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 62,
                "y": 98
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "323f4e4a-0701-4aab-87bf-b4263414782e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 17,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 26,
                "y": 98
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "a8cdf58a-29ad-498f-a2ad-75b4c026e893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 17,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "94c3bed0-bc28-4350-9f1d-ee59c595c336",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 17,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 250,
                "y": 170
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "4e8c1d3a-08cd-431e-9238-c490215f3c52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 17,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 245,
                "y": 170
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "e8caa361-6772-4eb4-adb2-2725a2fcd4ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 17,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 162,
                "y": 146
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "0d470fc5-b10f-4540-b512-b8cc2f265a87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 117,
                "y": 170
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "41105034-de6a-4758-873e-616ceb3447e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 92,
                "y": 50
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "83c88b57-039d-4cc3-a80d-ba3b0a485543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 35,
                "y": 74
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "a1d3153d-dcd6-46e7-aa0c-a9cadb510cde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 17,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 47,
                "y": 50
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "47a8e422-6a17-45ac-a502-ad76b15b10da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 17,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 50
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "cfed2e45-776b-4e51-b816-397c54cf72e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 17,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 17,
                "y": 50
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "60f79fa6-e98c-40f8-8c06-a824c005122a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 17,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 216,
                "y": 26
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "e5d52380-e1a1-45d5-81bb-e0f1190380c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 17,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 231,
                "y": 26
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "db692cf5-9864-4b9e-a33e-1b5ecf2d0d67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 125,
                "y": 170
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "6df4a84c-1d13-4c8a-8457-95ffa056b1b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 70,
                "y": 26
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "19d519f6-29c9-4661-90c0-cbe838fab679",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 126,
                "y": 74
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "69be5430-5507-422a-aa2b-71d22b87f74e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 87,
                "y": 74
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "474ca475-fc89-4815-b02b-f0c4a4893e3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 61,
                "y": 74
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "d5a408b9-c960-4373-990a-40e187f671b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 113,
                "y": 74
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "632eeb5a-809e-4918-b079-087e1099e0fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 174,
                "y": 50
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "02246897-245f-46c2-b3b3-004e9783f4e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 17,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 122,
                "y": 98
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "17050930-e979-474d-bdb9-602ff6551fd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 110,
                "y": 98
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "6899a549-6925-45ac-8707-b6c743aac8b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 98,
                "y": 98
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "73c9785c-efff-4fa1-8cdb-d4b9c87012e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 50,
                "y": 98
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "def2b205-2203-4a96-b87b-cd7f679562fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 38,
                "y": 98
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "5c34bede-e7df-47c8-8fba-c82b96ea12c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 98
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "6fcfe111-2dc8-4285-a93a-7a2db930c906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 170,
                "y": 98
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "24a93304-8d3b-41af-a28f-000afb5875ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 194,
                "y": 98
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "e9040cd1-0075-4189-a810-8ba95bea2f16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 17,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "255ac0c2-ee4f-44f0-add2-1abf94b93182",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "1cb0e94d-9fc8-4fd7-97ce-7e608884e993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 146
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "987ffb6f-812e-4223-8229-a3c0ded164d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 222,
                "y": 122
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "060dabc8-7eba-49f0-80a3-aef858626033",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 122
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "5137cb28-905c-4c54-877c-c523cf837c1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 46,
                "y": 122
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "0f0c2416-2e98-430f-9076-9eddf9eaa506",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 17,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 235,
                "y": 170
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "c4e5e798-431b-464a-9fb7-8317b3e7a9af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 17,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 240,
                "y": 170
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "e12b1d62-80d4-48e6-ab51-fadc5e600772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 17,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 232,
                "y": 146
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "80ad955a-ebe8-4339-8a15-20c3e3f776c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 109,
                "y": 170
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "769d630b-7408-4465-ba51-f18eaa11d60d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 57,
                "y": 122
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "7feee91e-a310-4d6a-b08f-9b9ecc920cdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 132,
                "y": 146
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "ac67adc2-a624-4c40-85fe-d8adec5b3573",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 68,
                "y": 122
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "0213e528-46fc-4995-8def-6edaec9be380",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 35,
                "y": 122
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "7a78f21a-bf7e-465c-b8d1-97c470b6412f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 90,
                "y": 122
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "935284bb-ea3c-41ad-aba8-a8fb05ea3220",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 101,
                "y": 122
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "88d65fa6-65bf-4884-871d-aa7362a04d9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 112,
                "y": 122
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "f86f9eb8-9b08-4a1c-a9db-7d7075f0f8fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 46,
                "y": 146
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "3aa0892b-08f8-46d8-b27b-9918debd5239",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 217,
                "y": 74
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "1457c93b-5b54-481f-90cc-27263aea30ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 182,
                "y": 146
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "4b0b66ff-cad2-4c61-bbf5-2ce0c2cb80e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 102,
                "y": 146
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "7f2cf2bf-ed47-4da7-8f31-ad3b47bab51e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 92,
                "y": 146
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "df9082d4-f5a5-4480-b08f-a979202daab4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 152,
                "y": 146
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "71a548a3-7b68-434b-9d34-56ace14b40d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 21,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 149,
                "y": 26
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "df8377c1-d4d1-4785-a7b5-1e585aeec380",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 225,
                "y": 50
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "8e14c014-e51d-4c14-ae6e-4022554adc66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 21,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 162,
                "y": 26
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "f5e9bb42-1218-46b9-a311-ac7fc1640350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "7dc2d7f3-c80e-4a46-95d5-9a5dbd888f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "fd8a4428-bd99-41e4-bbd5-c80c168d46a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "9bb1c88d-cec3-4224-ba2b-bbaf2eca95d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "cd8bc8ca-90f8-4758-ae3a-0fd4db900212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "17ff2eda-995e-43a4-a2b1-e1d0c5b11b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "593f65c0-5113-4732-9643-b952046b7be0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "11f73108-9db7-4aff-8717-41724022e6e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "108d9ab7-3148-414d-a6be-8ad01db2b364",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "0f0f0e46-d9e1-4629-a5e9-1e6efef8adeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "84d0af23-e4d2-48d6-9c2a-aed5b4e25505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "87c6dc02-38b3-4fb8-aa9a-ae2f55dd6737",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "6db465e0-e89a-45cb-88de-60e3879f0c0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "25eaffef-6689-45c1-be4b-90792ba003f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "b2cc8c87-d529-4699-bc75-25e0780fa7ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "710d18f6-2fab-4557-aab2-6a1301b85fe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "e2bc77e9-3699-492a-b520-6a37260fd92c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "3e3ed177-fc5d-42e2-b3c0-4787a5d2e14c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "c059580d-4dcd-44d0-8954-62f4692d62c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "12df4890-ff3d-4fd5-b9b1-2990be7b3b83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "9c3e8e95-f4e3-4d7e-a3c9-eb6614c77ced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "20a7845c-ff5d-447a-a7a7-4af465782a8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "3a29e210-85d2-420e-80a8-4126fce8c53e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "74c2423b-1091-467b-a83b-4ac08ea56bc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "5f8cc83e-5a83-4622-8325-fabb175953fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "432c1a98-996a-466b-97b4-5abdf9897742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "801b50e7-ec86-4889-be81-8793f7fa1caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "1ded08c1-ed52-4360-bce2-62190cac4a41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "b58a3af4-c523-429f-b274-85c31385b5bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "0b6854d4-8781-494a-9b62-e9778c89834e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "b74d0e89-d25c-410c-bd78-21a2ae354868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "b073653a-3163-4366-aa6e-ce27b16e7a18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "af22cf24-8dfe-437c-8e6b-915c2352a7be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "327c1d68-15a7-4ee6-847d-9d9d82026ad3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "5ed9663e-1f1d-4a74-a86e-1d1b66f9a5b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "472adf58-fa9a-4a35-ba8f-1d25923201b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "68447d38-d19c-48d6-aed7-5036e5684a2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "87d065e1-da28-4801-b2f8-1ae1124b9a3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "ffa953a3-40c5-420f-8769-3e992b6eeb35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "8f277f7d-6ddd-4c56-9f13-f8111d4ad0b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "179f85aa-b925-4600-9d37-2588a5c787e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "a0936cd6-1cba-4cb8-90c1-ece3df4f1867",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "3016d69a-47a6-4530-843c-d07397c1ad4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "404e71a4-b49f-4460-bfb4-51344849e33e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "b0c6c123-da44-4cf6-aed3-a75bb6213c30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "11d9829d-8fb0-49ed-b720-69265c9899e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "8d155463-6c89-4228-a7f8-9ac517d2a81d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "6ad10267-f8ed-44da-b767-3fd35edfad0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "20c3db60-21b9-43b6-9bbe-528898e34947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "d911436c-fd11-46e3-a57e-d9b0cdee6f56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "17bb2715-e97e-4b50-8cb2-4af8120040d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "6246f93c-79a7-424f-95d3-37757ae945cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "030df141-6c99-4f3f-9bc0-8a1c7773b106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "a3405c42-9fd0-49d6-b0bb-2006911c3c8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "ff71761e-53f6-4268-a6aa-2b9b4c1a7457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "58ca3b85-e09c-494e-a0ce-5d0a7f0cae07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "e4ee571e-a27c-40e4-ab94-deb2b7e6a966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "ff71446e-25f1-4491-a0a0-1f4452944a61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "b0acbd2c-4960-4bc8-b146-702f38f881a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "a07f036f-6872-4d6b-a79c-f7564da7ce5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "09724e62-2b90-4d2f-90b0-01ae97edebac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "340b95b8-6c6c-4f8f-81fa-20fa2a12dc99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "e990bb6d-3b1b-4fb0-90d9-7296271a7d62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "aaadfcbc-5109-43bb-aaf8-cf800ba20c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "3c0550b8-fb12-4b33-8d22-e07dcae28655",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "0aa14a7b-18ac-4ff0-8f74-ed96a3a8e495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "81eaa26f-e4ab-43c7-97c8-0019ae4da4bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "f05ebec6-9924-4db6-83ca-053717155a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "24fdad50-ec2f-4ed6-b3f9-cfcc27005c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "85b6a5c0-fd50-4e14-9d7b-e85f0fd51b6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "be1b9275-859f-4fed-941d-c4e29c8b2b64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "b710b2df-c081-4658-a168-73286c8462b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "c80e3350-b80a-4074-9444-1fa5fdaac6b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "984ccf7d-3918-4e1b-801a-734c5350786f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "79405d3c-0f45-4981-97d8-305026950f4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "d99a2cc4-dbc9-41de-bf60-9bcf73735939",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "866be8db-79c6-4470-9492-e200d7572ffa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "eb87b75e-5a73-477a-be2a-cd9d67be70c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "184ebf6e-ee72-4985-8389-05e734323c26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "8f658e99-90f8-4ca1-bdcf-febc23a46c76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "d3ebfaa7-abf9-484d-8f63-9e743b2dd156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "cffe5a8d-6ec1-4dc0-b443-9c9f7dcfa64d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "c2f48915-2259-4edf-9069-a8d65bec98d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "e8144700-95ab-4201-8520-4f24248f68fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "f0be78e8-778b-4ba5-830a-1e7048f3891d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "85acb761-80c6-4601-94d1-858010f89aa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "2b32befb-da6d-4be1-8227-4d0178826ddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "db8a53cc-fd36-48f7-87d1-b36b33ed3775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "7b859283-6e05-4811-8671-3b97c80eda26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 65
        },
        {
            "id": "d1a3cf5c-4649-4c3a-b3a1-837090b0a13f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 902
        },
        {
            "id": "05804c68-1154-4353-a5a4-3f8b9859fe97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 913
        },
        {
            "id": "a4b36c33-13c9-44d8-99c4-86dddd769fa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 916
        },
        {
            "id": "50801cf5-4974-4681-b648-03fb62797121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 923
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}