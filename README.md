# Project Name: GMS2_localization  
## version v1.0.0  
This is a collection of scripts to manage localization in a GameMaker Studio project  
(tested in GSM2 but should also work for GMS1)   

It uses a JSON file which includes all the strings from the game and also settings informations.
An example of use is provided.

Strong points: 
- it is not needed to know all the languages at compilation time. It possible to update the JSON file after the release of the 
executable in order to add new languages (see test 2).
- a default language can be defined (the language which contains all the texts of the game). A translation for another language is not necessary complete.
In this case, missing translations will be displayed in the default language.

# LOCALIZATION API
- scr_loc_init()
- scr_loc_deinit()
- scr_loc_translate()
- scr_loc_get_next_lang()
- scr_loc_get_prev_lang()
- scr_loc_set_current_lang()
- scr_loc_get_current_lang()

Functions used internally (can be used be should not be necessary)
- scr_loc_set_default_lang()
- scr_loc_get_default_lang()
- scr_loc_get_object_id()
- scr_loc_read_json_file()

Functions present but not ready for the moment
- scr_loc_read_csv_file() 


# HOW TO USE
One object of the project should be considered as the "localization controller" (not necessary a dedicated object).  
In the create event of this object, call the function scr_loc_init() with JSON file as parameter.  
In the destroy event of this object, call the function scr_loc_deinit().  

In the code, just call scr_loc_translate() with text_key as parameter.  
If the key is not found for the current language, it will returns the text for the defaut language.  
If the key does not exist for the default language too, it with return a string with key name : "?? key name ??".  

Except scr_loc_init() and scr_loc_deinit(), all the functions can be called from any object.  
But the localization object has to exist in the current room !!!  
Calling localization scripts before having call scr_loc_init() is not allowed (will return invalid strings).

# TESTS
Two tests are provided as examples.  
In the first test (test object 1), languages are fixed at compilation time. The game "knows" all the available languages.

In the second test (test object 2), languages are not fixed at compilation time. The game use the API to parse all the available languages.
In this example, Italian has been added after the release of the test application.

## TO DO NEXT ##
- create a package including only the scripts
- add a function to read a CSV file to generate the JSON file
- test in GMS1
- create more tests to check robustness (exple: call  )
- more detailled documentation (API, how to uses, ...)

