/// @desc select the sprite for requested language
/// @arg {enum} selected language
/// returns the adequate flag sprite index
var _selected_language = argument0;

switch (_selected_language)
{
	case game_lang_e.english:
		_flag_sprite = spr_flag_england;
		break;
	case game_lang_e.french:
		_flag_sprite = spr_flag_france;
		break;
	case game_lang_e.spanish:
		_flag_sprite = spr_flag_spain;
		break;
	case game_lang_e.portuguese:
		_flag_sprite = spr_flag_portugal;
		break;					
}

return(_flag_sprite);