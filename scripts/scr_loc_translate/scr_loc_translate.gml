/// @desc translate a key to the text in selected language
/// @arg {string} _textkey key of the text to display
/// returns the correct text string
/// NOTE: the localization controller objet is the object where scr_loc_init
///       and scr_loc_deinit are called
var _textkey = argument0;

// retrieve id of object responsible for localization
var loc_object = scr_loc_get_object_id();
if (loc_object == noone)
{
	return("!!NULL loc object");
}

// retrieve the text corresponding to key for the selected language
var retval = ds_map_find_value(loc_object.locvar_current_lang_map, _textkey);

if is_undefined(retval)
{
	var default_lang_map = ds_map_find_value(loc_object.locvar_all_lang_map, loc_object.locvar_default_lang);
	// if value not found for the requested language, use word for default language
	retval = ds_map_find_value(default_lang_map, _textkey);
	
	if is_undefined(retval)
	{
		// no value for default language, return textkey with ??
		retval = "?? " + _textkey + " ??"
	}
}

return(retval);
