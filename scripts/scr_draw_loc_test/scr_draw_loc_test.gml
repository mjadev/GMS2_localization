/// @desc Draw start menu
/// @arg test_number
var _test_nb = argument0;

var _pos_x     = 50; // starting X position
var _pos_y     = 50; // starting Y position
var _step_vert = 50;  // space between each line
var _x_off     = 450; // X offset
var _y_off     = 50;   // Y offset

draw_set_colour(c_white);
draw_set_font(fnt_test_menu);
draw_set_halign(fa_left);
draw_set_valign(fa_top);

var item_color = c_white;
draw_set_colour(item_color);

_pos_y += _step_vert;
draw_text(_pos_x + 350, _pos_y , scr_loc_translate("LanguageName"));

if (_test_nb == 1)
{
	 _pos_y += _step_vert;
	draw_text(_pos_x + 350, _pos_y , "TEST object 1: press flag to change language");
	_pos_y += _step_vert;
	draw_text(_pos_x + 350, _pos_y , "(press space for next test, esc to quit)");
}
else if (_test_nb == 2)
{
	_pos_y += _step_vert;
	draw_text(_pos_x + 350, _pos_y , "TEST object 2: press arrows to change language");
	_pos_y += _step_vert;
	draw_text(_pos_x + 350, _pos_y , "(press esc to quit)");
}

_pos_y += _step_vert;
draw_text(_pos_x , _pos_y + _y_off, "default language = " + scr_loc_get_default_lang()
	+ " / current language = " + scr_loc_get_current_lang());

_pos_y += _step_vert;
draw_text(_pos_x , _pos_y + _y_off, "test1: should be displayed in all languages");
draw_text(_pos_x + _x_off, _pos_y + _y_off, "1: " + scr_loc_translate("test1"));

_pos_y += _step_vert;
draw_text(_pos_x , _pos_y + _y_off, "test2: should be displayed in all languages");
draw_text(_pos_x + _x_off, _pos_y + _y_off, "2: " + scr_loc_translate("test2"));

_pos_y += _step_vert;
draw_text(_pos_x , _pos_y + _y_off, "test3: should be displayed in all languages");
draw_text(_pos_x + _x_off, _pos_y + _y_off, "3: " + scr_loc_translate("test3"));

_pos_y += _step_vert;
draw_text(_pos_x , _pos_y + _y_off, "test4: use default translation (english) ");
draw_text(_pos_x + _x_off, _pos_y + _y_off, "4: " + scr_loc_translate("test4"));

_pos_y += _step_vert;
draw_text(_pos_x , _pos_y + _y_off, "test5: no translation available even for default");
draw_text(_pos_x + _x_off, _pos_y + _y_off, "5: " + scr_loc_translate("test5"));
