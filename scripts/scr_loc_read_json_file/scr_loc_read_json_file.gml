/// @desc this script open a JSON file and analyze it to extract supported languages
/// @arg {string} file file name
/// returns the decoded JSON file (format = ds_map)
var _file = argument0;

var lang_file;
var lang_data;

if (0)
{
	// open lang file and read it line by line
	// result stored in a string (better to use buffers ? see end of script)
	lang_file = file_text_open_read(working_directory + "\\" + _file);
	lang_data = "";
	while (!file_text_eof(lang_file))
	{
		  lang_data += file_text_read_string(lang_file);
		  file_text_readln(lang_file);
	}
	file_text_close(lang_file);
	// decode json string to ds_map
	_lang_ds_map = json_decode(lang_data);
}
else
{
	// using buffer (growing buffer)
	// buffer size is arbitrarely fixed to 1024 but it can be tuned
	var mystring_buffer = buffer_create(1024, buffer_grow, 1);
	lang_file = file_text_open_read(working_directory + "\\" + _file);
	while (!file_text_eof(lang_file))
	{
			lang_data = file_text_read_string(lang_file);
			buffer_write(mystring_buffer, buffer_text, lang_data);
			
			// display current buffer position
			// show_debug_message("buffer tell = " + string(buffer_tell(mystring_buffer)));
			
			var buf_size = buffer_get_size(mystring_buffer);
	    lang_data = file_text_read_string(lang_file);
	    file_text_readln(lang_file);
	}
	file_text_close(lang_file);
	
	// display last buffer position
	// show_debug_message("buffer end = " + string(buffer_tell(mystring_buffer)));
	
	// return to buffer start position
	buffer_seek(mystring_buffer, buffer_seek_start, 0);
	// show_debug_message("buffer start = " + string(buffer_tell(mystring_buffer)));
	
	// decode json string to ds_map
	var buf_content = buffer_read(mystring_buffer, buffer_string);
	_lang_ds_map = json_decode(buf_content);
  // buffer no more needed, dstroy it
	buffer_delete(mystring_buffer);
}

return (_lang_ds_map);
