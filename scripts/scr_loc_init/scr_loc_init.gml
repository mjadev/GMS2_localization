/// @desc create the ds_list to decode JSON localization file
/// @arg {string} file file name
/// !! this script has to be called from the localization object
/// it initializes the object variables used for localization
var _file = argument0;

// list of variables used for localization
// Global variables:
//  - global.locvar_object_id : id of object used to manage localization
// Localization object variables:
//  - locvar_lang_list : ds_list which contains supported languages
//  - locvar_all_lang_map : ds_map which contains all translation datas
//  - locvar_default_lang : default language to use if no translation available
//  - locvar_current_lang : current language
//                          for the current language

var settings_present = 0;

// save id of localization controller object
global.locvar_object_id = id;

// decode JSON language file (into a ds_map)
locvar_all_lang_map = scr_loc_read_json_file(_file);

// create an empty ds_list of supported languages
locvar_lang_list = ds_list_create();

// count number of languages in JSON file and populate the ds_list
var size = ds_map_size(locvar_all_lang_map);
var key = ds_map_find_first(locvar_all_lang_map);
var i = 0;
// parse the map to find all keys (languages or settings)
do{
	if (key == "Settings")
	{
		show_debug_message("settings key found");
		settings_present = 1;
	}
	else
	{
		show_debug_message("language key found:" + key);
		// add lang to the list
		ds_list_add(locvar_lang_list, key);
	}
	key = ds_map_find_next(locvar_all_lang_map, key);
	i++;
} until (i == (size - 0));

// initialize default and current languages
var default_lang;
var current_lang;

if (settings_present == 1)
{
	// use settings info to set default and current languages
	// select a language
  var settings_section = ds_map_find_value(locvar_all_lang_map, "Settings");

	default_lang = ds_map_find_value(settings_section, "default");
	if is_undefined(default_lang)
	{
		// default language is the first found (can be changed later)
		default_lang = ds_map_find_first(locvar_all_lang_map);
		show_debug_message("error: default lang settings undefined");
	}

	current_lang = ds_map_find_value(settings_section, "current");
	if is_undefined(current_lang)
	{			
		// current language is the first found (can be changed later)
		current_lang = ds_map_find_first(locvar_all_lang_map);
		show_debug_message("error: current lang settings undefined");
	}
}
else
{
  // no settings section in JSON file
	// default and current language set to the first found
	default_lang = ds_map_find_first(locvar_all_lang_map);
	current_lang = default_lang;
}

show_debug_message("default lang set to " + default_lang);
scr_loc_set_default_lang(default_lang);

show_debug_message("current lang set to " + current_lang);
scr_loc_set_current_lang(current_lang);
