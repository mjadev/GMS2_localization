// @desc scr_loc_get_object_id
// return id of localization object if it exists
// return noone if it does not exist
var loc_object = global.locvar_object_id;
if (loc_object == noone)
{
	show_debug_message("Warning, localization object not initialized !!!");
}

return(loc_object);
