/// @desc return current language

// retrieve id of object responsible for localization
var loc_object = scr_loc_get_object_id();
if (loc_object != noone)
{
	return(loc_object.locvar_current_lang);
}
else
{
	return("");
}
