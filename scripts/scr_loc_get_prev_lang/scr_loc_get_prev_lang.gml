/// @desc select previous language (loop to last if needed)

var loc_object = scr_loc_get_object_id();
if (loc_object == noone)
{
	return(""); //TODO
}

var size = ds_list_size(loc_object.locvar_lang_list);
show_debug_message("size = " + string(size));
var current_idx = ds_list_find_index(loc_object.locvar_lang_list, loc_object.locvar_current_lang);
show_debug_message("current_idx = " + string(current_idx));
if (current_idx == 0)
{
	current_idx = size - 1;
}
else
{
	current_idx--;
}
show_debug_message("current_idx = " + string(current_idx));
var prev_lang = ds_list_find_value(loc_object.locvar_lang_list, current_idx);
show_debug_message("new default lang = " + prev_lang);

return(prev_lang);