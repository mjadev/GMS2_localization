/// destroy the ds_list created when JSON file has been decoded
/// !! this script has to be called from the localization object
/// it clears the object variables used for localization

ds_map_destroy(locvar_all_lang_map);
ds_list_destroy(locvar_lang_list);
global.locvar_object_id = noone;