/// @desc select next language (loop to beginning if needed)

// retrieve id of object responsible for localization
var loc_object = scr_loc_get_object_id();
if (loc_object == noone)
{
	return(""); //TODO
}

var size = ds_list_size(loc_object.locvar_lang_list);
show_debug_message("size = " + string(size));
var current_idx = ds_list_find_index(loc_object.locvar_lang_list, loc_object.locvar_current_lang);
show_debug_message("current_idx = " + string(current_idx));
if (current_idx == (size -1))
{
	current_idx = 0;
}
else
{
	current_idx++;
}
show_debug_message("current_idx = " + string(current_idx));
var next_lang = ds_list_find_value(loc_object.locvar_lang_list, current_idx);
show_debug_message("next lang = " + next_lang);

return(next_lang);

