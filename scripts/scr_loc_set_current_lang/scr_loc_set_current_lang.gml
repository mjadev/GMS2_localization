/// @desc select current language to use
/// @arg {string} current language to set

var _current_language = argument0;

// retrieve id of object responsible for localization
var loc_object = scr_loc_get_object_id();
if (loc_object != noone)
{
	loc_object.locvar_current_lang = _current_language;
	loc_object.locvar_current_lang_map = ds_map_find_value(loc_object.locvar_all_lang_map, _current_language);

	//TODO check it is a valid lang sring, otherwise warning and do not apply change
}