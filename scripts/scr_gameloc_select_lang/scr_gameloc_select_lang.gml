/// @desc select a language and set object datas
/// @arg {enum} selected language
/// return a string of selected language
var _selected_language = argument0;

var _lang_string = "";

switch (_selected_language)
{
	case game_lang_e.english:
		_lang_string = "English";
		break;
	case game_lang_e.french:
		_lang_string = "French";
		break;
	case game_lang_e.spanish:
		_lang_string = "Spanish";
		break;
	case game_lang_e.portuguese:
		_lang_string = "Portuguese";
		break;					
}

return(_lang_string);