/// @desc select default language to use if no translation available for
///       the current language
/// @arg {string} default language

var _default_language = argument0;

// retrieve id of object responsible for localization
var loc_object = scr_loc_get_object_id();
if (loc_object != noone)
{
	loc_object.locvar_default_lang = _default_language;

	//TODO check it is a valid lang sring, otherwise warning and do not apply change
}

