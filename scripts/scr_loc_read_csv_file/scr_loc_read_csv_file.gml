/// @desc this script open a CSV file and analyze it to extract supported languages
/// @arg {string} file file name
/// returns the decoded CSV file (format = ds_grid)
var _file = argument0;

var lang_file;
var lang_data;

// open csv file
file_grid = load_csv(_file);	
var ww = ds_grid_width(file_grid);
var hh = ds_grid_height(file_grid);
for (var i = 0; i < ww; i++;)
{
  for (var j = 0; j < hh; j++;)
	{
      //show_debug_message(string(file_grid[# i, j]));
			show_debug_message("("+string(i)+","+string(j)+"):"+file_grid[# i, j]);
	}
}


//return (_lang_ds_grid);
